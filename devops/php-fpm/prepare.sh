#!/usr/bin/env bash

set -e

devops/php-fpm/healthcheck 60

# Prepare application
bin/console cache:clear
bin/console doctrine:migration:migrate -n --allow-no-migration
