#!/usr/bin/env bash

set -e

xdebug_ini=/usr/local/etc/php/conf.d/xdebug.ini

if [ -f ${xdebug_ini} ]; then
    sed -i -r "s/^xdebug\.remote_host\=.*$/xdebug.remote_host=$(ip route|awk '/default/ { print $3 }')/" ${xdebug_ini}
fi

su -m -s /bin/bash -c 'cd /srv; devops/php-fpm/prepare.sh' - www-data

php-fpm
