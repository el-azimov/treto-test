<?php


namespace ApiBundle\Middleware;


use Core\ICommandBus;
use Core\IHandleResolver;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;

class ResponseSerializer
{
    private $commandBus;

    private $handleResolver;

    public function __construct(
        ICommandBus $commandBus,
        IHandleResolver $handleResolver
    )
    {
        $this->commandBus = $commandBus;
        $this->handleResolver = $handleResolver;
    }

    public function onKernelView(GetResponseForControllerResultEvent $event)
    {
        if (false === strpos($event->getRequest()->headers->get('Accept'), 'json')) {
            return;
        }

        $controllerResult = $event->getControllerResult();

        if (is_object($controllerResult) && $this->handleResolver->supports($controllerResult)) {
            $event->setControllerResult($this->commandBus->handle($controllerResult));
        }

        $request = $event->getRequest();
        $result = $event->getControllerResult();

        switch ($request->getMethod()) {
            case Request::METHOD_POST:
                $statusCode = Response::HTTP_CREATED;
                break;
            case Request::METHOD_GET:
                $statusCode = Response::HTTP_OK;
                break;
            default:
                $statusCode = Response::HTTP_NO_CONTENT;
        }

        $event->setResponse(empty($result) ? new Response() : new JsonResponse($result, $statusCode));
    }
}