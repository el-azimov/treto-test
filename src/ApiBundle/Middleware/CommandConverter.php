<?php


namespace ApiBundle\Middleware;


use ApiBundle\ConstructResolver;
use Core\IHandleResolver;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;

class CommandConverter implements ParamConverterInterface
{
    private $handleResolver;

    private $constructResolver;

    public function __construct(IHandleResolver $handleResolver, ConstructResolver $constructResolver)
    {
        $this->handleResolver = $handleResolver;
        $this->constructResolver = $constructResolver;
    }


    public function apply(Request $request, ParamConverter $configuration): bool
    {
        try {
            $command = $this->constructResolver->resolve($request, $configuration->getClass());
        } catch (\Exception $e) {
            return false;
        }

        $request->attributes->set($configuration->getName(), $command);
        return true;
    }

    public function supports(ParamConverter $configuration): bool
    {
        return $this->handleResolver->supports($configuration->getClass());
    }
}
