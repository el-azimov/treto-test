<?php


namespace ApiBundle\Controller;


use Picture\GetPicturesCommand;
use Picture\NewPicturesFromListCommand;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PictureController extends Controller
{
    /**
     * @Route(path="/picture/new", methods={"POST"})
     */
    public function newPictureAction(NewPicturesFromListCommand $command)
    {
        return $command;
    }

    /**
     * @Route(path="/pictures", methods={"GET", "HEAD"})
     */
    public function getPictures(GetPicturesCommand $command)
    {
        return $command;
    }
}