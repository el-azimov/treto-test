<?php


namespace ApiBundle;


use Symfony\Component\HttpFoundation\Request;

class ConstructResolver
{
    public function resolve(Request $request, $className)
    {
        $ref = new \ReflectionClass($className);

        $arguments = [];

        foreach ($ref->getConstructor()->getParameters() as $param) {
            $value = $request->get($param->getName());

            if ('SplFileInfo' === $param->getType()) {
                $value = $request->files->get($param->getName());
            } elseif (null !== $value) {
                $value = $this->typeCast($param->getType(), $value);
            }


            $arguments[$param->getName()] = $value;
        }

        return $ref->newInstanceArgs($arguments);
    }

    private function typeCast(string $type, $value)
    {
        switch ($type) {
            case 'int':
                return (int)$value;

            case 'bool':
                return (bool)$value;

            case 'DateTime':
                try {
                    return new \DateTime($value);
                } catch (\Exception $e) {
                    return null;
                }

            default:
                return (string)$value;
        }
    }
}
