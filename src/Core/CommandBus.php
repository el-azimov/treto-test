<?php


namespace Core;


class CommandBus implements ICommandBus
{
    private $resolver;

    public function __construct(IHandleResolver $resolver)
    {
        $this->resolver = $resolver;
    }

    public function handle($command)
    {
        if (!$this->resolver->supports($command)) {
            throw new NoSuchCommandException($command);
        }

        list ($handler, $method) = $this->resolver->resolve($command);

        return $handler->$method($command);
    }
}
