<?php


namespace Core;


interface ICommandBus
{
    /**
     * @return array|null
     */
    public function handle($command);
}
