<?php


namespace Core;


interface IRepo
{
    public function add($object);

    public function remove($object);

    public function refresh($object);
}
