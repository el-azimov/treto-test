<?php


namespace Core;


use Exception;

class NoSuchCommandException extends \LogicException
{
    public function __construct($command = null, $code = null, Exception $previous = null)
    {
        if (is_object($command)) {
            $command = get_class($command);
        }

        $message = sprintf('No command with the name of "%s"', $command);
        parent::__construct($message, $code, $previous);
    }

}
