<?php


namespace Core;


abstract class PaginationCommand
{
    const DEFAULT_PAGE=1;
    const DEFAULT_PAGE_SIZE=20;

    public $page;

    public $pageSize;

    public function __construct(int $page = null, int $pageSize = null)
    {
        $this->page = $page ?: static::DEFAULT_PAGE;
        $this->pageSize = $pageSize ?: static::DEFAULT_PAGE_SIZE;
    }
}