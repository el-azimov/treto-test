<?php


namespace Core;


interface IHandleResolver
{
    public function resolve($command): array;

    public function supports($command): bool;
}
