<?php


namespace Picture;


use Atom\Uploader\Model\Uploadable;

class PictureFile
{
    use Uploadable;
}