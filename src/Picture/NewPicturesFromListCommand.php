<?php


namespace Picture;


class NewPicturesFromListCommand
{
    public $list;

    public function __construct(\SplFileInfo $list = null)
    {
        $this->list = $list;
    }
}