<?php


namespace Picture;


use Gregwar\Image\Image;
use GuzzleHttp\Client;
use GuzzleHttp\Promise\PromiseInterface;
use GuzzleHttp\Psr7\Response;

class PictureHandler
{
    const PICTURE_HEIGHT = 200;

    private $pictureRepo;

    private $client;

    private $imageHandler;

    public function __construct(IPictureRepo $pictureRepo, Client $client, Image $imageHandler)
    {
        $this->pictureRepo = $pictureRepo;
        $this->client = $client;
        $this->imageHandler = $imageHandler;
    }

    public function getPictures(GetPicturesCommand $command)
    {
        return $this->pictureRepo->getAll($command->page, $command->pageSize);
    }

    public function newPicturesFromList(NewPicturesFromListCommand $command)
    {
        $list = file_get_contents($command->list);
        $list = explode("\n", str_replace("\r", '', $list));
        $list = $this->removeNotUniqueUrls($list);

        $pictures = [];

        foreach ($this->fetchPictures($list) as $url => $file) {
            /** @var $file \SplFileInfo */
            $this->crop($file);
            $this->setWatermark($file);
            $size = getimagesize($file->getPathname());
            $pictureFile = new PictureFile($file);
            $picture = new Picture($pictureFile, $url, $size[0]);
            $this->pictureRepo->add($picture);
            $pictures[] = $picture->toArray();
        }

        return $pictures;
    }

    private function crop(\SplFileInfo $file)
    {
        $path = $file->getPathname();

        $this->imageHandler->open($path)
            ->cropResize(null, self::PICTURE_HEIGHT)
            ->save($path, $file->getExtension());
    }

    private function setWatermark(\SplFileInfo $file)
    {
        $path = $file->getPathname();
        $waterMark = $this->imageHandler->open(__DIR__ . '/watermark.png');
        $image = $this->imageHandler->open($path);

        $image
            ->merge($waterMark, max(0, $image->width() - 110), 165)
            ->save($path, $file->getExtension());
    }

    private function removeNotUniqueUrls(array $list)
    {
        $newList = [];

        foreach ($list as $url) {
            if (empty($url) || 0 !== strpos($url, 'http') || null !== $this->pictureRepo->findByUrl($url)) {
                continue;
            }

            $newList[] = $url;
        }

        return $newList;
    }

    private function fetchPictures(array $list): array
    {
        $promises = [];
        $pictures = [];

        foreach ($list as $sourceUrl) {
            $promise = $this->client->getAsync($sourceUrl);
            $promises[$sourceUrl] = $promise;
            $promise->then(function (Response $response) use (&$pictures, $sourceUrl) {
                if (false === strpos($response->getHeaderLine('Content-Type'), 'image')) {
                    return;
                }

                $file = uniqid(sys_get_temp_dir() . '/') . '.' . pathinfo($sourceUrl, PATHINFO_EXTENSION);
                file_put_contents($file, $response->getBody());

                $pictures[$sourceUrl] = new \SplFileInfo($file);
            });
        }

        foreach ($promises as $promise) {
            /** @var PromiseInterface $promise */
            $promise->wait();
        }

        return $pictures;
    }
}