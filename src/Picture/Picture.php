<?php


namespace Picture;


class Picture
{
    private $id;
    
    private $file;

    private $sourceUrl;

    private $width;

    public function __construct(PictureFile $file, string $sourceUrl, int $width)
    {
        $this->file = $file;
        $this->sourceUrl = $sourceUrl;
        $this->width = $width;
    }

    public function toArray()
    {
        return [
            'id' => $this->id,
            'file' => $this->file->toArray(),
            'sourceUrl' => $this->sourceUrl,
            'width' => $this->width
        ];
    }
}