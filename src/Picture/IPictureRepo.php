<?php


namespace Picture;


use Core\IRepo;

interface IPictureRepo extends IRepo
{
    /**
     * @return Picture|null
     */
    public function findByUrl(string $url);

    public function getAll(int $page, int $pageSize): array;
}