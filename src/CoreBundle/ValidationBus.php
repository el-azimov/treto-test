<?php


namespace CoreBundle;


use Core\CommandBus;
use Core\ICommandBus;
use CoreBundle\Exception\BadCommandException;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ValidationBus implements ICommandBus
{
    private $commandBus;
    
    private $validator;
    
    public function __construct(CommandBus $commandBus, ValidatorInterface $validator)
    {
        $this->commandBus = $commandBus;
        $this->validator = $validator;
    }

    /**
     * @return array|null
     */
    public function handle($command)
    {
        $validationErrors = $this->validator->validate($command);

        if (0 !== count($validationErrors)) {
            $errors = [];

            foreach ($validationErrors as $error) {
                /** @var $error ConstraintViolation */
                $errors[] = [
                    'property' => $error->getPropertyPath(),
                    'message' => $error->getMessage(),
                ];
            }

            throw new BadCommandException($errors, $command);
        }
            
        return $this->commandBus->handle($command);
    }
}