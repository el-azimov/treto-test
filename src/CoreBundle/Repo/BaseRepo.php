<?php


namespace CoreBundle\Repo;


use Core\IRepo;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Picture\Picture;

abstract class BaseRepo implements IRepo
{
    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function add($object)
    {
        $this->em->persist($object);
    }

    public function remove($object)
    {
        $this->em->remove($object);
    }

    public function refresh($object)
    {
        $this->refresh($object);
    }

    protected function createQueryBuilder(string $entityName, string $alias, string $indexBy = null): QueryBuilder
    {
        return $this->em
            ->createQueryBuilder()
            ->select($alias)
            ->from($entityName, $alias, $indexBy);
    }

    protected function createPagination(Query $query, int $page, int $pageSize): array
    {
        $query->setFirstResult($pageSize * $page - 1);
        $query->setMaxResults($pageSize);

        $pagination = new Paginator($query);
        $items = [];

        foreach ($pagination->getIterator() as $item) {
            /** @var $item Picture */
            $items[] = $item->toArray();
        }

        $total = $pagination->count();
        $pagesCount = ceil($total / $pageSize);

        return compact('items', 'pagesCount', 'total');
    }
}
