<?php


namespace CoreBundle\Repo;


use Picture\IPictureRepo;

class PictureRepo extends BaseRepo implements IPictureRepo
{
    public function findByUrl(string $url)
    {
        return $this->em
            ->createQuery('SELECT p from Picture:Picture p WHERE p.sourceUrl = :url')
            ->setParameter('url', $url)
            ->getOneOrNullResult();
    }

    public function getAll(int $page, int $pageSize): array
    {
        $query = $this->em->createQuery('SELECT p FROM Picture:Picture p');

        return $this->createPagination($query, $page, $pageSize);
    }
}