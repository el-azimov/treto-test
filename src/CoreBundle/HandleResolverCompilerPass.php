<?php


namespace CoreBundle;


use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class HandleResolverCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $definition = $container->findDefinition('core.handle_resolver');
        $taggedService = $container->findTaggedServiceIds('core.handle_resolver');

        $handlers = [];

        foreach ($taggedService as $id => $tags) {
            $reflection = new \ReflectionClass($container->findDefinition($id)->getClass());

            foreach ($reflection->getMethods(\ReflectionMethod::IS_PUBLIC) as $method) {
                if (1 !== $method->getNumberOfParameters()
                    || 0 === strpos($method->getName(), '__')
                ) {
                    continue;
                }

                $param = $method->getParameters()[0];
                $className = $param->getClass()->getName();
                $handlers[$className] = [$id, $method->getName()];
            }
        }

        $definition->replaceArgument(1, $handlers);
    }
}
