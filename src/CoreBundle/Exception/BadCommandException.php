<?php


namespace CoreBundle\Exception;


use Exception;

class BadCommandException extends \RuntimeException
{
    private $errors;

    public function __construct(array $errors, $command, int $code = null, Exception $previous = null)
    {
        if (is_object($command)) {
            $command = get_class($command);
        }

        $message = sprintf(sprintf('Bad parameters for command "%s"', $command));

        parent::__construct($message, $code, $previous);
    }

    public function getErrors(): array
    {
        return $this->errors;
    }
}