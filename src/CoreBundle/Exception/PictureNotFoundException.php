<?php


namespace CoreBundle\Exception;


use Exception;

class PictureNotFoundException extends NotFoundException
{
    public function __construct(int $id = null, int $code = null, Exception $previous = null)
    {
        $message = sprintf('Picture with id "%s" not found.', $id);

        parent::__construct($message, $code, $previous);
    }
}