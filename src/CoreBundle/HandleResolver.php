<?php


namespace CoreBundle;


use Core\IHandleResolver;
use Symfony\Component\DependencyInjection\ContainerInterface;

class HandleResolver implements IHandleResolver
{
    private $container;

    private $handlers;

    public function __construct(ContainerInterface $container, array $handlers)
    {
        $this->container = $container;
        $this->handlers = $handlers;
    }

    public function resolve($command): array 
    {
        list($serviceId, $method) = $this->handlers[$this->toClassName($command)];

        return [$this->container->get($serviceId), $method];
    }

    public function supports($command): bool
    {
        return isset($this->handlers[$this->toClassName($command)]);
    }

    private function toClassName($object)
    {
        if (is_object($object)) {
            return get_class($object);
        }

        return $object;
    }
}
